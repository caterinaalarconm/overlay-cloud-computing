# Construcción manual de una red entre contenedores

### Antes de ejecutar 

> Por comodidad ejecutar el script en modo root.

- Es necesario comprobar si existe , y no está comentada, la línea `net.ipv4.ip_forward=1` en el fichero `/etc/sysctl.conf` para así habilitar el reenvio de paquetes.
- Deshabilitar el firewall de linux con el comando `ufw disable`.



### Ejecución
El repositorio constiene 2 scripts diferentes 

- `run_overlay.sh`
- `container_config.sh`

y un fichero `Dockerfile` para generar una imagen de Ubuntu configurada sobre la que lanzar los contenedores.

El script `run_overlay.sh` se encarga de la configuración incial (creación de la imagen, la interfaz bridge, etc) y es el script al que se debe llamar para realizar toda la configuración.


Por su parte el script `container_config.sh`es llamado por `run_overlay.sh` y se encarga de crear y configurar los containers. En caso de ya haber ejecutado el run y querer añadir un nuevo container, se puede ejecutar directamente este script.


### Comprobaciones

- Comprobar el acceso desde un nodo accesible desde el host

Ejecutar en el host:

```sh
$ docker exec -it [nombre del container] nc -l [puerto asignado al container]
```
Ejecutar en un nodo diferente:

```sh
$ nc [ip del host] [puerto asignado al container]
```