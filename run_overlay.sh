#!/bin/bash

###### Construcción manual de una red entre contenedores ######

docker build -t ov-image .
echo "Image 'ov-image' created"
echo -n "Enter host physical interface name: " 
read eth

ip link add bridge_d type bridge
ip addr add 10.1.1.1/24 brd + dev bridge_d

ip link set bridge_d up
echo "Bridge interface 'bridge_d' created"


while :
do
    ./container_config.sh
    echo -n "Create another container?(y/n) " 
    read cnt
    if [ $cnt = "n" ];
    then
        break
    fi
done


bash -c "echo 0 > /proc/sys/net/bridge/bridge-nf-call-iptables"
echo "Bridge iptables disabled"

iptables -t nat -A POSTROUTING -s 10.1.1.0/24 -o $eth -j MASQUERADE
echo "Done!"


