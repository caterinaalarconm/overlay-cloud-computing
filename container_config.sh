#!/bin/bash

echo -n "Enter container name: " 
read name

docker run -dit --name $name --network none --privileged ov-image bash

echo -n "Enter a number for the veth: " 
read int1

veth1=veth$int1
veth2=veth$((int1+1))

ip link add $veth1 type veth peer name $veth2
ip link set $veth1 master bridge_d
ip link set dev $veth1 up

ip link set $veth2 netns $(docker inspect --format '{{.State.Pid}}' $name)

echo -n "Created veth pair --> $veth1 , $veth2" 

echo -n "Enter last ip num (10.1.1.X): " 
read num

ip=10.1.1.$num

docker exec -d $name ip addr add $ip/24 dev $veth2
docker exec -d $name ip link set dev $veth2 up

docker exec -d $name ip route add 0.0.0.0/0 via 10.1.1.1

echo -n "Asign a port to the container: " 
read port
echo -n "Enter host IP: " 
read host

echo "Adding iptables rule ..."
iptables -t nat -A PREROUTING -p tcp --dport $port -d $host -j DNAT --to-destination $ip

echo "Container $name created!"